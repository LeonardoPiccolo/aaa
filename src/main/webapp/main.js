let carrinho = document.querySelectorAll('.buy-bnt');
let produtos = [
    {
        nome: "futon Alaska",
        value: 179,
        noCarrinho: 0
    },
    {
        nome: "Cama para gatos",
        value: 60,
        noCarrinho: 0
    },
    {
        nome: "Caminha",
        value: 30,
        noCarrinho: 0
    },
    {
        nome: "Futon para filhotes",
        value: 36,
        noCarrinho: 0
    },
    {
        nome: "Pratinho de Ração",
        value: 20,
        noCarrinho: 0
    },
    {
        nome: "Antipulgas para gatos",
        value: 30,
        noCarrinho: 0
    },
    {
        nome: "Coleira de couro",
        value: 50,
        noCarrinho: 0
    }
];

for (let i=0 ; i < carrinho.length ; i++) {
    carrinho[i].addEventListener('click',() => {carrinhoMontante(produtos[i]);});
}

function carregarMontante(){
    let produtoMontante = localStorage.getItem('numeroCarrinho');
    
    if(produtoMontante){
        document.querySelector(".carrinho").textContent = produtoMontante;
    }
};

function carrinhoMontante(produto){
    let produtoMontante = localStorage.getItem('numeroCarrinho');
    produtoMontante = parseInt(produtoMontante);
    
    if(produtoMontante){
        localStorage.setItem('numeroCarrinho', produtoMontante + 1);
        document.querySelector(".carrinho").textContent = produtoMontante + 1;
    }
    else{
        localStorage.setItem('numeroCarrinho', 1);
        document.querySelector(".carrinho").textContent = 1;
    }
    setarItens(produto);
}

function setarItens(produto){
    let itensCarrinho = localStorage.getItem('produtosNoCarrinho');
    itensCarrinho = JSON.parse(itensCarrinho);
    
    if(itensCarrinho !== null){
        itensCarrinho[produto.nome].noCarrinho += 1;
    }
    else{
        produto.noCarrinho = 1;
        itensCarrinho = {[produto.nome]: produto};
    }
    
    localStorage.setItem("produtosNoCarrinho", JSON.stringify(itensCarrinho));
}

carregarMontante();
console.log("funfou");