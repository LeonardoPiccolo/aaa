<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form de registro</title>

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
     crossorigin="anonymous">
  
    <link rel="https://pro.fontawesome.com/releses/v5.10.0//css/.all.css"
    integrity="AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJOvKxVfELGroGkvsg+p"
    crossorigin="anonymous"/>

    <link rel="stylesheet" href="style.css">
    
</head>
<body>
    <div id="main-container">
        <h1>registrar produto</h1>
        <form id="register-form" action="produtoServlet" method="post">
            <fieldset>
            <div class="full-box">
                <label>nome</label>
                <input type="text" name="nome">
            </div>
            <div class="full-box">
                <label>raça</label>
                <input type="text" name="raca">
            </div>
            <div class="full-box">
                <label>Valor entrada</label>
                <input type="text" name="valorEnt">
            </div>
            <div class="full-box">
                <label>valor venda</label>
                <input type="text" name="valorVend">
            </div><!-- comment -->
             <div class="full-box">
                <label>status</label>
                <input type="text" name="status">
            </div>   
            <div class="full-box">
                <label>descrição</label>
                <input type="text" name="desc">
            </div>
            <div class="full-box">
            <div class="full-box">
                <label>registro</label>
                <input type="text" name="registro">
            </div>
            <div class="full-box">
                <label>quantidade</label>
                <input type="text" name="quantidade">
            </div>
            <div class="full-box">
                <label>fornecedor</label>
                <input type="text" name="fornecedor">
            </div>    
                <input type="checkbox" name="agreement" id="agreement">
                <label for="agreement" id="agreement-label">li e aceito os <a href="#">termos de uso</a></label>
            </div>
            <div class="full-box">
                
                <input type="submit" id="btn-submit" value="Registrar">
        
            </div>
        </fieldset>
        </form>
    </div>
    <p class="error-validation template"></p>

  

        <script src="script.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
      integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" 
      crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
      integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/"
        crossorigin="anonymous"></script>    
        
</body>
</html>
