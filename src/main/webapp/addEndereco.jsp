<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form de registro</title>

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
     integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
     crossorigin="anonymous">
  
    <link rel="https://pro.fontawesome.com/releses/v5.10.0//css/.all.css"
    integrity="AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJOvKxVfELGroGkvsg+p"
    crossorigin="anonymous"/>

    <link rel="stylesheet" href="style.css">
    
</head>
<body>
    <div id="main-container">
        <h1>Cadastre seu endereço</h1>
        <form id="register-form" action="registroServlet" method="post">
            <fieldset>
            
            <div class="full-box">
                <label>Endereço</label>
                <input type="text" name="endereco" placeholder="Digite seu endereço">
            </div>
                
            <div class="full-box">
                <label>CEP</label>
                <input type="number" name="CEP" placeholder="Digite seu CEP">
            </div>
                
            <div class="full-box">
                <label>Bairro</label>
                <input type="text" name="bairro" placeholder="Digite seu bairro">
            </div>
                
            <div class="full-box">
                <label>Cidade</label>
                <input type="text" name="cidade" placeholder="Digite sua cidade">
            </div>
                
            <div class="full-box">
                <label>UF</label>
                <select name="UF">
                    <option selected disabled value="">Selecione</option>
                    <option>DF</option>
                    <option>GO</option>
                    <option>MT</option>
                    <option>RS</option>
                    <option>MS</option>
                    <option>SC</option>
                    <option>PR</option>
                    <option>SP</option>
                    <option>RJ</option>
                    <option>ES</option>
                    <option>MG</option>
                    <option>BA</option>
                    <option>SE</option>
                    <option>AL</option>
                    <option>PE</option>
                    <option>PB</option>
                    <option>RN</option>
                    <option>CE</option>
                    <option>PI</option>
                    <option>MA</option>
                    <option>TO</option>
                    <option>AP</option>
                    <option>PA</option>
                    <option>RR</option>
                    <option>AM</option>
                    <option>AC</option>
                    <option>RO</option>
                </select>
                
            </div>
            
            <div class="full-box">
                <input type="submit" id="btn-submit" value="Registrar">
            </div>
                
        </fieldset>
        </form>
    </div>
    <p class="error-validation template"></p>

  

        <script src="script.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
      integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" 
      crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
      integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/"
        crossorigin="anonymous"></script>    
        
</body>
</html>