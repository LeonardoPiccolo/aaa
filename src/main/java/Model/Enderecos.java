package Model;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

public class Enderecos {
	
	private int idEndereco;
    private String CEP;
    private String endereco;
    private String bairro;
    private String cidade;
    private String uf;
    
        public int getIdEndereco() {
		return idEndereco;
	}
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
        public String getCep() {
		return CEP;
	}
	public void setCep(String CEP) {
		this.CEP = CEP;
	}
        public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
        public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
        public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
        public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
}
