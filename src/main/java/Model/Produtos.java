package Model;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Leonardo
 */
@Getter
@Setter
public class Produtos {

    private int idProduto;
    private String nomeProduto;
    private String raca;
    private String valorEntrada;
    private String valorVenda;
    private String statusProduto;
    private String descricao;
    private String dataRegistro;
    private String quantidade;
    private String fornecedor;
    private String imagem;

    public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
    public String getNomeProduto() {
		return nomeProduto;
	}
    public void setRaca(String raca) {
		this.raca = raca;
	}
	public String getRaca() {
		return raca;
	}
	public void setValorEntrada(String valorEntrada) {
		this.valorEntrada = valorEntrada;
	}
        public String getValorEntrada() {
		return valorEntrada;
	}
        public void setValorVenda(String valorVenda) {
		this.valorVenda = valorVenda;
	}
        public String getValorVenda() {
		return valorVenda;
	}
		public String getStatusProduto() {
			return statusProduto;
		}
		public void setStatusProduto(String statusProduto) {
			this.statusProduto = statusProduto;
		}
		public String getDescricao() {
			return descricao;
		}
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		public String getDataRegistro() {
			return dataRegistro;
		}
		public void setDataRegistro(String dataRegistro) {
			this.dataRegistro = dataRegistro;
		}
		public String getFornecedor() {
			return fornecedor;
		}
		public void setFornecedor(String fornecedor) {
			this.fornecedor = fornecedor;
		}
		public String getQuantidade() {
			return quantidade;
		}
		public void setQuantidade(String imagem) {
			this.quantidade = quantidade;
		}
                public String getImagem() {
			return imagem;
		}
		public void setImagem(String imagem) {
			this.imagem = imagem;
		}
}