/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Sthefany
 */
@Getter
@Setter
public class Clientes {

    private int idCliente;
    private String nome;
    private String cpf;
    private String dataNascimento;
    private String sexo;
    private String senha;
    private String email;
    private String contato;
    private String statusCliente;

    
        public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
        public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
        public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
        public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
        public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
        public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
        public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
        public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
        public String getStatusCliente() {
		return statusCliente;
	}
	public void setStatusCliente(String statusCliente) {
		this.statusCliente = statusCliente;
	}
}
