package DAO;

import Model.Cartoes;
import java.sql.Connection;
import java.sql.PreparedStatement;

import Utils.Conexao;

public class CartaoDAO {
	
	static Connection conexao;
    static Conexao minhaConexao;
    
    public static boolean cadastrarCartao(Cartoes cartao) {
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();
           
            String sql = "insert into Cartoes ( nomeTitular, numeroCartao, dataVencimento, CVV)"
                    + "values (?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cartao.getNomeTitular());
            instrucaoSQL.setString(2, cartao.getNumeroCartao());
            instrucaoSQL.setString(3, cartao.getDataVencimento());
            instrucaoSQL.setString(4, cartao.getCvv());


            int linhasAfetadas = instrucaoSQL.executeUpdate();
            

            if (linhasAfetadas > 0) {
                status = true;
                
            } else {
                throw new Exception();

            }

                        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
   
}
