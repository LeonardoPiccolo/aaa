package DAO;

import Model.Enderecos;
import Utils.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EnderecoDAO {
    
    static Connection conexao;

    static Conexao minhaConexao;
   
    
    public static boolean cadastrarEndereco(Enderecos endereco) {
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();
           
            String sql = "insert into Enderecos ( CEP, endereco, bairro, cidade, uf) "
                    + "values (?,?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, endereco.getCep());
            instrucaoSQL.setString(2, endereco.getEndereco());
            instrucaoSQL.setString(3, endereco.getBairro());
            instrucaoSQL.setString(4, endereco.getCidade());
            instrucaoSQL.setString(5, endereco.getUf());

            int linhasAfetadas = instrucaoSQL.executeUpdate();
            

            if (linhasAfetadas > 0) {
                status = true;
                
            } else {
                throw new Exception();

            }

                        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean alterarEndereco(Enderecos endereco) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "UPDATE Enderecos SET CEP = ?, endereco = ?, bairro = ?, cidade = ?, uf = ? WHERE idEndereco = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, endereco.getCep());
            instrucaoSQL.setString(2, endereco.getEndereco());
            instrucaoSQL.setString(3, endereco.getBairro());
            instrucaoSQL.setString(4, endereco.getCidade());
            instrucaoSQL.setString(5, endereco.getUf());
            instrucaoSQL.setInt(6, endereco.getIdEndereco());
            
                    
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean removerEndereco(String endereco) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "delete from Enderecos where idEndereco = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, endereco);
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean validarEndereco(Enderecos endereco) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Enderecos where CEP = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, endereco.getCep());
            
            rs = instrucaoSQL.executeQuery();
    
            if (rs != null) {
                rs.close();
                System.out.println("Cliente existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Cliente não existe.");
            }
            
            conexao.close();
            
            
        } catch (Exception e) {
            System.out.println("Erro na consulta.");
            
        }
        
        return status;
        
    }
    
    public static Enderecos consultarEndereco(String endereco) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Enderecos end = new Enderecos();
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Enderecos where idEndereco = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setString(1, endereco);
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
            	end.setIdEndereco(rs.getInt("idEndereco"));
                end.setCep(rs.getString("CEP"));
                end.setEndereco(rs.getString("endereco"));
                end.setBairro(rs.getString("bairro"));
                end.setCidade(rs.getString("cidade"));
                end.setUf(rs.getString("uf"));
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Cliente existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Cliente não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return end;
        
    }    
    
    public static ArrayList<Enderecos> listarEnderecos() {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Enderecos> listaEndereco = new ArrayList<Enderecos>();
        
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Enderecos";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) {
                
            	Enderecos end = new Enderecos();
                
            	end.setIdEndereco(rs.getInt("idEndereco"));
                end.setCep(rs.getString("CEP"));
                end.setEndereco(rs.getString("endereco"));
                end.setBairro(rs.getString("bairro"));
                end.setCidade(rs.getString("cidade"));
                end.setUf(rs.getString("uf"));
                listaEndereco.add(end);
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na listagem");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return listaEndereco;
        
    }
    
    public static int pegarId(Enderecos endereco) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        int idEndereco = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select idEndereco from Enderecos where CEP = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, endereco.getCep());
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
            	idEndereco = rs.getInt("idEndereco");
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return idEndereco;
        
    }
}
