
package DAO;

import Model.Produtos;
import Utils.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sthefany
 */
//NÃO MECHER ESSE É O BASE !! //
public class ProdutoDAO {
    
    static Connection conexao;

    static Conexao minhaConexao;
   
    
    
    
    
    public static boolean cadastrarProdutos(Produtos produto) {
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();
           
            String sql = "insert into Produtos ( nomeProduto, raca, valorEntrada, valorVenda, statusProduto, descricao, dataRegistro, quantidade, fornecedor, imagem) "
                    + "values (?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, produto.getIdProduto());
            instrucaoSQL.setString(2, produto.getNomeProduto());
            instrucaoSQL.setString(3, produto.getRaca());
            instrucaoSQL.setString(4, produto.getValorEntrada());
            instrucaoSQL.setString(5, produto.getValorVenda());
            instrucaoSQL.setString(6, produto.getStatusProduto());
            instrucaoSQL.setString(7, produto.getDescricao());
            instrucaoSQL.setString(8, produto.getDataRegistro());
            instrucaoSQL.setString(9, produto.getQuantidade());
            instrucaoSQL.setString(10, produto.getFornecedor());
            instrucaoSQL.setString(11, produto.getImagem());



            int linhasAfetadas = instrucaoSQL.executeUpdate();
            

            if (linhasAfetadas > 0) {
                status = true;
                
            } else {
                throw new Exception();

            }

                        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean alterarProdutos(Produtos produto) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "UPDATE Produtos SET nomeProduto = ?, raca = ?, valorEntrada = ?, valorVenda = ?, statusProduto = ?, descricao = ?, dataRegistro = ?, quantidade = ?, fornecedor = ?, imagem = ? WHERE idProduto = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, produto.getNomeProduto());
            instrucaoSQL.setString(2, produto.getRaca());
            instrucaoSQL.setString(3, produto.getValorEntrada());
            instrucaoSQL.setString(4, produto.getValorVenda());
            instrucaoSQL.setString(5, produto.getStatusProduto());
            instrucaoSQL.setString(6, produto.getDescricao());
            instrucaoSQL.setString(7, produto.getDataRegistro());
            instrucaoSQL.setString(8, produto.getQuantidade());
            instrucaoSQL.setString(9, produto.getFornecedor());
            instrucaoSQL.setString(10, produto.getImagem());
            instrucaoSQL.setInt(11, produto.getIdProduto());

            
                    
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean removerProdutos(String produto){
        
        minhaConexao = new Conexao();       
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "delete from Produtos where idProduto = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, produto);
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public boolean validarProdutos(Produtos produto) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Produtos where idProduto = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, produto.getIdProduto());
            
            rs = instrucaoSQL.executeQuery();

    
            if (rs != null) {
                rs.close();
                System.out.println("Cliente existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Cliente não existe.");
            }
            
            conexao.close();
            
            
        } catch (Exception e) {
            System.out.println("Erro na consulta.");
            
        }
        
        return status;
        
    }
    
    public static Produtos consultarProdutos(String produto) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Produtos prod = new Produtos();
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Produtos where idProduto = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setString(1, produto);
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
            	prod.setIdProduto(rs.getInt("idProduto"));
                prod.setNomeProduto(rs.getString("nomeProduto"));
                prod.setRaca(rs.getString("raca"));
                prod.setValorEntrada(rs.getString("valorEntrada"));
                prod.setValorVenda(rs.getString("valorVenda"));
                prod.setStatusProduto(rs.getString("statusProduto"));
                prod.setDescricao(rs.getString("descricao"));
                prod.setDataRegistro(rs.getString("dataRegistro"));
                prod.setQuantidade(rs.getString("quantidade"));
                prod.setFornecedor(rs.getString("fornecedor"));
                prod.setImagem(rs.getString("imagem"));

            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Cliente existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Cliente não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return prod;
        
    }    
    
    public static ArrayList<Produtos> listarProdutos() {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Produtos> listaProdutos = new ArrayList<Produtos>();
        
        try {
            
           // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select * from Produtos";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) {
                
            	Produtos prod = new Produtos();
                
                prod.setIdProduto(rs.getInt("idProduto"));
                prod.setNomeProduto(rs.getString("nomeProduto"));
                prod.setRaca(rs.getString("raca"));
                prod.setValorEntrada(rs.getString("valorEntrada"));
                prod.setValorVenda(rs.getString("valorVenda"));
                prod.setStatusProduto(rs.getString("statusProduto"));
                prod.setDescricao(rs.getString("descricao"));
                prod.setDataRegistro(rs.getString("dataRegistro"));
                prod.setQuantidade(rs.getString("quantidade"));
                prod.setFornecedor(rs.getString("fornecedor"));
                prod.setImagem(rs.getString("imagem"));
                listaProdutos.add(prod);

                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na listagem");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return listaProdutos;
        
    }
    
    public static int pegarId(Produtos produto) {
        
        minhaConexao = new Conexao();       
        boolean status = false;
        int idProduto = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            // Class.forName(DRIVER);
            conexao = minhaConexao.abrirConexao();

            String sql = "select idProduto from Produtos where nomeProduto = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, produto.getNomeProduto());
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
                idProduto = rs.getInt("idCliente");
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return idProduto;
        
    }    
} 
