package Controller;

import java.util.ArrayList;
import java.util.List;
import Model.Clientes;
import DAO.ClienteDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name="loginServlet", urlPatterns={"/loginServlet"})
public class loginServlet extends HttpServlet {
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    	
    	Clientes cliente = new Clientes();
    	ClienteDAO dao = new ClienteDAO();
        
        String email = request.getParameter("Email");
        String senha = request.getParameter("Senha");
        cliente.setEmail(email);
        cliente.setSenha(senha);
        
        
        if(dao.validarCliente(cliente)) {
        	response.sendRedirect("inicio.jsp");
        }
        if((email == "admin@admin.com")&&(senha == "petshop1234")) {
        	response.sendRedirect("admin.jsp");
        }
        else {
        	response.sendRedirect("index.jsp");
        }
    }
}

