package Utils;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao 
{
    public static String status = "Não conectado";
                   
    public static String driver = "com.mysql.cj.jdbc.Driver";   //DRIVER DO MYSQL A PARTIR DA VERSÃO 8                   
    
    public static String server = "pipetshop-4sem.co3ielfeiodz.us-east-2.rds.amazonaws.com"; //STRING DE CONEXÃO COM A AMAZON
    public static String database = "petshop";               //NOME DO BANCO DE DADOS
     
    public static String login = "admin";                        //USUÁRIO DO BANCO DE DADOS   
    
    public static String senha = "PIPETSHOP1234";                       //SENHA DE ACESSO
   
    public static String url = "jdbc:mysql://pipetshop-4sem.co3ielfeiodz.us-east-2.rds.amazonaws.com:3306"; //UPDATE PARA ESSA STRING DE CONEXÃO

    static java.sql.Connection CONEXAO = null;

    
        
    

    

    
    public static java.sql.Connection abrirConexao() throws ClassNotFoundException, SQLException 
    {

        url = "jdbc:mysql://" + server + ":3306/" + database + "?useTimezone=true&serverTimezone=UTC&useSSL=false";


            try 
            {
                Class.forName(driver);
                CONEXAO = DriverManager.getConnection(url, login, senha);
                
                if (CONEXAO != null) 
                {
                    status = "Conexão realizada com sucesso!";
                } 
                else 
                {
                    status = "Não foi possivel realizar a conexão";
                }
            } 
            catch (ClassNotFoundException e) 
            {  
              throw new ClassNotFoundException("O driver expecificado nao foi encontrado.");
            } 
            catch (SQLException e) 
            {  
              throw new SQLException("Erro ao estabelecer a conexão (Ex: login ou senha errados).");
            }
        
            try 
            {
                if (CONEXAO.isClosed()) 
                {
                  CONEXAO = DriverManager.getConnection(url, login, senha);
                }
            } 
            catch (SQLException ex) 
            {
              throw new SQLException("Falha ao fechar a conexão.");
            }

        return CONEXAO;
    }
    
     /**
     * Método responsável por Fechar a conexão com o Banco de Dados.
     * 
     * @return boolean informando se a conexão está fechada ou aberta
     * @throws SQLException 
     */
    public static boolean fecharConexao() throws SQLException 
    {
        boolean response = false;
        try 
        {
            if (CONEXAO != null) {
                if (!CONEXAO.isClosed()) {
                    CONEXAO.close();
                }
            }
            status = "Não conectado";
            response = true;
        } 
        catch (SQLException e) 
        {
            response = false;
        }
        return response;
    }
    public static String getStatusConexao() 
    {
        System.out.println("Status: " + status);
        return status;
    }
}
